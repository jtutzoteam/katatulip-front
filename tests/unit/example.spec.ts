import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import ListUsers from '@/components/ReferentialCard.vue';

describe('ReferentialCard.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message';
    const wrapper = shallowMount(ListUsers, {
      propsData: { msg },
    });
    expect(wrapper.text()).to.include(msg);
  });
});
