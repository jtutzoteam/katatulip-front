#!/usr/bin/env bash

set -o nounset
set -o errexit

IMAGE_NAME=jtutzo/katatulip-front:dev

npm run build
docker build -t $IMAGE_NAME .
