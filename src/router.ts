import Vue from 'vue';
import Router from 'vue-router';
import Referential from './views/Referential.vue';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'referential',
            component: Referential,
        },
    ],
});
